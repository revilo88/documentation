<!-- _coverpage.md -->

![logo](_media/Reading-528.png)

# docsify <small>4.7.0</small>

> Ein magischer Generator für Dokumentationsseiten.

* Einfach und wenig Speicherbedarf (~19kB gzipped)
* Keine statischen HTML Dateien
* Mehrere Themes
